#!/bin/bash

datenow=$(date +'%Y%m%d%H%M%S')
path="/home/hanif/log/metrics_$datenow.log"

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > $path

awk '/Mem/{printf "%u,%u,%u,%u,%u,%u,",$2,$3,$4,$5,$6,$7 }
' <(free -m) >> $path

awk '/Swap/{printf "%u,%u,%u," ,$2,$3,$4 }
' <(free -m) >> $path

awk '{printf "%s,%s" ,$2,$1 }
' <(du -sh /home/hanif) >> $path

chmod 700 $path
