#!/bin/bash

mkdir forensic_log_website_daffainfo_log
awk '
    /2022:00/{++a}
    /2022:01/{++a}
    /2022:02/{++a}
    /2022:03/{++a}
    /2022:04/{++a}
    /2022:05/{++a}
    /2022:06/{++a}
    /2022:07/{++a}
    /2022:08/{++a}
    /2022:09/{++a}
    /2022:10/{++a}
    /2022:11/{++a}
    /2022:12/{++a}
    END {
        rata_rata=a/13;
        print "Rata-rata serangan adalah sebanyak", rata_rata, "requests per jam"
    }
' log_website_daffainfo.log > ratarata.txt
mv ratarata.txt forensic_log_website_daffainfo_log
sed 's/"/ /g' log_website_daffainfo.log > newlog.log
awk '
    {a[$1] += 1;} 
    END {
        for (i in a) printf("%s adalah IP yang paling banyak mengakses server yakni dengan requests sebanyak %d\n", i, a[i]);}' newlog.log | sort -n | tail -n 1 > result.txt
awk '
    /curl/{++jumlah_req_curl}
    END {
        print "Ada", jumlah_req_curl, "requests yang menggunakan curl sebagai user-agent"
    }
' log_website_daffainfo.log > result2.txt
cat result2.txt >> result.txt
awk '
    /2022:02/{print $1}
' newlog.log > result3.txt
cat result3.txt >> result.txt
mv result.txt forensic_log_website_daffainfo_log
