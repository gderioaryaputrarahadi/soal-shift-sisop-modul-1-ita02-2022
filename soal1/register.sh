#!/bin/bash 

[ ! -d "./users" ] && mkdir users

echo "Welcome to register!" 
NOW=$(date +"%m/%d/%Y %H:%M:%S")

while true;
do
 
	read -p 'Username: ' USERNAME;  
	stty -echo 
	read -p 'Password: ' PASSWORD; 
	stty echo 

  # check if username already exist
  	[[ $USERNAME == `grep -w $USERNAME ./users/user.txt | cut -d " " -f1` ]] && echo "$NOW REGISTER: ERROR User already exists" >> ./users/log.txt && echo "User $USERNAME already exist"
  
  # check if password meet the criteria
  	[[ $PASSWORD =~ [A-Z] && $PASSWORD =~ [a-z] && $PASSWORD =~ [0-9] && ${#PASSWORD} -ge 8 && "$PASSWORD" != "$USERNAME" ]] && {
	  echo "$USERNAME $PASSWORD" >> ./users/user.txt && 
	  echo "$NOW REGISTER: INFO User $USERNAME registered successfully" >> ./users/log.txt && 
	  echo "" && echo "Register success" ; exit 1
	} || echo "password tidak memenuhi kriteria"
done
