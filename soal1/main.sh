#!/bin/bash 

# sudo apt install curl zip unzip -y

echo "Login" 
NOW=$(date +"%m/%d/%Y %H:%M:%S")
DATE_NOW=$(date +"%Y-%m-%d")

while true;
do
	read -p 'Username: ' USERNAME;  
	stty -echo 
	read -p 'Password: ' PASSWORD; 
	stty echo 

    [[ $USERNAME == `grep -w $USERNAME ./users/user.txt | cut -d " " -f1` ]] && {
        [[ ! $PASSWORD == `grep -w $USERNAME ./users/user.txt | cut -d " " -f2` ]] && 
        echo "$NOW LOGIN: ERROR Failed login attempt on user $USERNAME" >> ./users/log.txt &&
        echo "" && echo "Password salah" || {
            [[ $PASSWORD == `grep -w $USERNAME ./users/user.txt | cut -d " " -f2` ]] && 
            echo "$NOW LOGIN: INFO User $USERNAME logged in" >> ./users/log.txt ; break
        } || echo "Error"
    } || echo "Username tidak terdaftar"
done

# download 
download(){
    # Download section
    for (( i=1; i<=$DOWNLOAD; i=i+1 ));do
        file_name="PIC_$(($x+$i))"
        [[ $(($x+$i)) -lt 10 ]] && file_name="PIC_0$(($x+$i))"
        curl -o ./$current_date_folder/$file_name https://loremflickr.com/320/240
    done
    cd $current_date_folder
    zip -r --password $PASSWORD $current_date_folder "PIC_"*
    rm -rf "PIC_"*
    cd ..
}

# MAIN
while true;
do
    echo ""
    echo "command"
    read -p '>>' COMMAND

    if [[ "$COMMAND" == "att" ]];then
        attempt=$(grep LOGIN ./users/log.txt | grep -c -w $USERNAME)
        echo "Jumlah percobaan login dari user $USERNAME = $attempt"
    
    elif [[ "$COMMAND" == "dl"* ]];then
        
        user_folder=`ls ./ | grep -w *_${USERNAME}`
        current_date_folder=${DATE_NOW}_${USERNAME}

        str_count=`grep dl <<< "$COMMAND" | cut -d " " -f2`
        DOWNLOAD="$(($str_count))"

        [[ -d ./$user_folder ]] && [[ ! $user_folder == $current_date_folder ]] && mv $user_folder  $current_date_folder || {
            [[ ! -d $user_folder ]] && mkdir "$current_date_folder" 2>/dev/null
        }
        
        # unzip file and count existing downloaded content
        [[ -f "${user_folder}/${user_folder}.zip" ]] && unzip -P $PASSWORD "${user_folder}/${user_folder}.zip" -d ./$user_folder/
        x=$(($(ls ./$current_date_folder | grep -c PIC)))

        download
    else
        echo "command not found"
        read -p 'do you want to quit [Y/n]?' quit_quest
        while true;
        do
            [[ $quit_quest == "n" ]] && break || [[ ! $quit_quest == "Y" ]] && continue || exit 1
        done
    fi
done