# Soal Shift SisOp Modul 1 ITA02 2022
Anggota Kelompok:

-Asima Prima Yohana Tampubolon 5027201009

-Gde Rio Aryaputra Rahadi 5027201063

-Muhammad Hanif Fatihurrizqi 5027201068

# Soal 1

# 1a. 
Pada sub bagian ini, kita diminta untuk membuat suatu script bernama register.sh. Di dalam script ini, kita akan membuat suatu sistem register. pada sistem register ini, apabila user telah berhasil melakukan register, maka username dan password akan tersimpan di suatu file bernama user.txt. Di mana, file tersebut berada di dalam folder bernama users. Pada sub bagian ini juga kita disuruh untuk membuat suatu sistem login di dalam script bernama main.sh.

## Penyelesaian
Pertama sekali kami membuat script bernama register.sh dan main.sh. 

## Register.sh
Pada script register.sh, mula-mula kami melakukan pengecekan dengan menggunakan command -d untuk mengecek keberadaan directory users tersebut. Apabila tidak ada maka akan membuat directory users dan apabila ada maka akan next. sesudah itu, dengan menggunakan while kami membuat program input username dan password baru di dalamnya program while agar program terus meminta inputan sampai input yang dimasukkan sesuai dengan ketentuan. Ketika berhasil melakukan register maka username dan password akan dimasukkan ke dalam file user.txt dengan menggunakan operator >>. 

## Kendala  
Pada tahap ini, kami mengalami error dimana apabila kami langsung menggunakan >>./users/user.txt maka akan terjadi error. Hal ini terjadi karena kami belum membuat folder bernama users oleh sebab itu, di awal program akhirnya kami membuat terlebih dulu pengecekan directory dengan menggunakan -d.

## Main.sh
Mirip dengan register, awalnya kami menggunakan while = true agar program terus meminta input. Pada case ini, saya melakukan pengecekan dengan menggunakan command if [[ ! $PASSWORD == `grep -w $USERNAME ./users/user.txt | cut -d " " -f1` ]]. Disini saya menggunakan grep untuk melakukan pencocokan dengan username di user.txt. lalu, saya menggunakan cut -d ""-f1 agar input username yang dicocokkan hanyalah pada kolom pertama. karena pada soal ini, Kami menggunakan pemisah spasi. Apabila tidak ada username yang sesuai maka akan mencetak username tidak ditemukan.Begitu juga dengan password, kami menggunakan syntax yang sama tapi dengan kolom yang kedua atau f2. Dan apabila sesuai, maka akan breakdan lanjut pada soal program selanjutnya. Pada case selanjutnya juga begitu, kami melakukan pengecekan dengan menggunakan command yang sama. Dan apabila password tidak sesuai maka akan mencetak Password salah. 

Percobaan register:

![](img/1.1.png)

Berhasil masuk di user.txt

![](img/1.2.png)

Berhasil login

![](img/1.3.png) 

# 1.b 
Pada sub bagian ini, password yang nantinya akan digunakan hendaknya sesuai dengan kriteria yang telah dicantumkan.

## Penyelesaian 

## Hidden password 
Cara yang kami gunakan yaitu dengan menggunakan command stty -echo. Dengan menggunakan command ini, nantinya input yang kita ketik tidak akan terlihat. Lalu, untuk mengembalikan agar input terlihat adalah dengan cara menggunakan stty echo. 

## Minimal 8 karakter
Kami menggunakan command -ge 8 yang artinya panjang digit password harus lebih besar atau sama dengan 8. Pada case ini, variable menggunakan # karena  agar yang dihitung berupa panjang digit dari password bukan nilai dari password.

## Memiliki minimal 1 huruf kapital dan 1 huruf kecil
Kami menggunakan command $PASSWORD =~ [A-Z] && $PASSWORD =~ [a-z] yang artinya anggota himpunan password harus berisi huruf kapital dan huruf kecil.

## Alphaneumeric
Alphaneumeric artinya gabungan huruf dan angka sehingga, kami cukup menambahkan  $PASSWORD =~ [0-9] karena pada kriteria sebelumnya telah diwajibkan untuk menggunakan huruf.

## Tidak boleh sama dengan username
Case yang terakhir, sama dengan case sebelumnya kami cukup menambahkan "$PASSWORD" != "$USERNAME" yang artinya password tidak boleh sama dengan username.
Semua case diatas kami gabungkan dalam 1 pengkondisian sehingga, apabila kondisinya sudah benar, akan melakukan input username dan pasaword ke dalam file user.txt. 
Lalu, apabila ternyata belum memenuhi kriteria, maka akan terus meminta inputan dan input yang sebelunya diisi tidak akan disimpan di dalam user.txt.

Apabila password tidak memenuhi kriteria ketika register

![](img/1.4.png) 

# 1.c 
Pada bagian ini, kita diminta untuk mencatat setiap aksi di dalam sistem login ataupun register yang nantinya akan disimpan di dalam file log.txt
 
## Penyelesaian
Untuk mengerjakan bagian ini, pertama sekali kita deklarasi NOW=$(date +"%m/%d/%Y %H:%M:%S") pada register.sh dan main.sh untuk mendapatkan time stamp. Sesudah itu, untuk masing masing kondisi password yang telah kita buat sebelumnya, kita cukup menambahkan pada masing masing kondisi.

## register.sh
## Apabila username telah ada 
Maka akan ditambahkan command echo "$NOW REGISTER: ERROR User already exists" >> ./users/log.txt
dimana nantinya statement tersebut akan langsung disimpan di log.txt
## Apabila berhasil register
Maka cukup menambahkan command echo "$NOW REGISTER: INFO User $USERNAME registered successfully" >> ./users/log.txt 

## main.sh
## Apabila password salah
Command yang ditambahkan adalah  echo "$NOW LOGIN: ERROR Failed login attempt on user $USERNAME" >> ./users/log.txt
## Apabila berhasil login
Command yang ditambahkan dalam pengkondisian adalah echo "$NOW LOGIN: INFO User $USERNAME logged in" >> ./users/log.txt

Tampilan log.txt Ketika baru saja login menggunakan akun Prima

![](img/1.5.png)

# 1d. 
Pada bagian ini, kita diminta untuk membuat 2 command pada program.

## dl N 
Pada command ini akan terjadi download gambar dari  https://loremflickr.com/320/240 dengan jumlah gambar sesuai dengan N. Lalu, hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Masing masing gambar juga akan diberi nama dengan format PIC_XX dengan nomor berurutan dimulai dari 01,02,dst. Setelah berhasil melakukan download maka folder akan otomatis di zipsesuai dengan format nama folder. Ketika melakukan zip, program akan otomatis menerima input password terlebih dahulu. Apabila terdapat file zip baru dengan nama yang sama maka file zip akan di unzip terlebih dahulu lalu disatukan dengan gambar baru lalu di zip kembali.

## att
Pada command ini kita diminta untuk menghitung jumlah percobaan login yang berhasil ataupun tidak dari user yang sedang login saat ini.

## Penyelesaian
Untuk membuat command ini, pertama sekali kami deklarasi variable DATE_NOW=$(date +"%Y-%m-%d"). Agar kami dapat dengan mudah melakukan rename file dan folder foto dengan mudah. Lalu, kami menggunakan while= true agar program terus meminta input command setelah login.  Setelah itu, dilakukan pengkondisian if sebanyak 3.

## att
Apabila input adalah att maka program akan mencari kata kunci LOGIN pada file log.txt lalu menghitung banyaknya percobaan login dengan menghitung banyaknya kata yang sama dengan variabel USERNAME. Setelah dihitung, maka akan ditampilkan.

    if [[ "$COMMAND" == "att" ]];then
        attempt=$(grep LOGIN ./users/log.txt | grep -c -w $USERNAME)
        echo "Jumlah percobaan login dari user $USERNAME = $attempt" 

Ketika menggunakan command att

![](img/1.6.png) 

-dl N
Apabila input adalah dl maka program akan deklarasi:
variable user_folder : isi berupa list folder dari user.
variable surrent_date_folder : isi berupa format penamaan yang telah disesuaikan dengan DATE_NOW
variable str_count : isi berupa jumlah N. Dimana N adalah jumlah foto yang ingin di download.
variable DOWNLOAD : isi berupa jumlah integer dari str_count

    elif [[ "$COMMAND" == "dl"* ]];then
        
        user_folder=`ls ./ | grep -w *_${USERNAME}`
        current_date_folder=${DATE_NOW}_${USERNAME}

        str_count=`grep dl <<< "$COMMAND" | cut -d " " -f2`
        DOWNLOAD="$(($str_count))"

Sesudah dilakukan deklarasi, maka akan dilakukan pengecekan ada tidaknya directory user_folder dan penamaan user_folder apabila file sudah ada dan namanya tidak sama maka nama user_folder akan diganti dengan current_date_folder. Lalu, apabila file tidak ada maka akan membuat directory terlebih dahulu dengan format nama menggunakan tanggal hari ini.
        [[ -d ./$user_folder ]] && [[ ! $user_folder == $current_date_folder ]] && mv $user_folder  $current_date_folder || {
            [[ ! -d $user_folder ]] && mkdir "$current_date_folder" 
        }
Setelah itu, dilakukan pengecekan lagi. Apabila ada folder zip dan tidak maka akan melakukan unzip dengan meminta input password terlebih dahulu menggunakan command -P $PASSWORD.

        # unzip file and count existing downloaded content
        [[ -f "${user_folder}/${user_folder}.zip" ]] && unzip -P $PASSWORD "${user_folder}/${user_folder}.zip" -d ./$user_folder/

Lalu, disini terdapat deklarasi x dimana x adalah jumlah dari folder PIC dari user yang sedang login 
        x=$(($(ls ./$current_date_folder | grep -c PIC)))

Setelah itu, program memanggil fungsi download
        download

Di dalam fungsi download, kami menggunakan for loop agar penamaan file sesuai dengan jumlah input atau pertambahan file 
download(){
    # Download section
    for (( i=1; i<=$DOWNLOAD; i=i+1 ));do
        file_name="PIC_$(($x+$i))"
Karena penamaan file harus minimal 2 digit maka menggunakan command -lt khusus nilai dibawah 10       
        [[ $(($x+$i)) -lt 10 ]] && file_name="PIC_0$(($x+$i))"
Sesudah itu, dilakukan download dari sumber dengan menggunakan fungsi curl
        curl -o ./$current_date_folder/$file_name https://loremflickr.com/320/240
    done
Setelah semua nama file telah sesuai maka akan file akan di zip. 
    cd $current_date_folder
    zip -r --password $PASSWORD $current_date_folder "PIC_"*
    rm -rf "PIC_"*
    cd ..
}
## Kendala:
Pada step ini, kami mengalami kendala ketika run curl, zip dan unzip. Tetapi dapat diselesaikan dengan cara install terlebih dahulu
menggunakan command #sudo apt install curl zip unzip -y di awal program main agar tidak error.

Ketika menggunakan command dl N

![](img/1.7.png) 


# Soal 2

# 2a.

## Analisa
Pada subsoal ini diminta untuk membuat script yang ketika dijalankan akan membuat folder bernama forensic_log_website_daffainfo_log

## Penyelesaian
Untuk membuat folder tersebut digunakan command 
    mkdir forensic_log_website_daffainfo_log
mkdir yang merupakan singkatan dari make directory akan membentuk folder dengan nama folder yang diinputkan setelah command tersebut.

# 2b.

## Analisa
Pada subsoal ini script diminta mencari rata-rata serangan per jam dari file log website daffa.info kemudian hasil rata-rata tersebut dimasukkan ke dalam sebuah file bernama ratarata.txt yang nantinya akan dipindahkan ke folder yang dibuat pada subsoal 2a.

## Penyelesaian
    awk '
    /2022:00/{++a}
    /2022:01/{++a}
    /2022:02/{++a}
    /2022:03/{++a}
    /2022:04/{++a}
    /2022:05/{++a}
    /2022:06/{++a}
    /2022:07/{++a}
    /2022:08/{++a}
    /2022:09/{++a}
    /2022:10/{++a}
    /2022:11/{++a}
    /2022:12/{++a}
    END {
        rata_rata=a/13;
        print "Rata-rata serangan adalah sebanyak", rata_rata, "requests per jam"
    }
    ' log_website_daffainfo.log > ratarata.txt

Untuk mencari rata-rata dari serangan maka dengan memanfaatkan awk dapat dilakukan "scan" pada file log website yang tersedia, pada kasus ini digunakan kriteria scan /2002:jam/. Contoh: /2002:00/ untuk scan serangan pada jam 00:00. Dan disini dideklarasikan variabel a sebagai penyimpan jumlah serangan yang discan pada tiap jam secara kumulatif. Kemudian hasil akhirnya akan dibagi dengan 13 (total 13 jam) dan output akan dimasukkan ke file ratarata.txt. Setelah itu file ratarata.txt akan dipindahkan ke folder yang telah dibuat sebelumnya dengan menggunakan command mv ratarata.txt forensic_log_website_daffainfo_log. 

# 2c.

## Analisa
Pada subsoal ini script diminta untuk mencari IP yang paling banyak melakukan request pada server kemudian menampilkan alamat ip tersebut beserta jumlah request yang dilakukan IP tersebut. Kemudian hasilnya dimasukkan ke file result.txt yang nantinya akan dipindahkan ke folder yang dibuat sebelumnya

## Penyelesaian
    sed 's/"/ /g' log_website_daffainfo.log > newlog.log
    awk '
    {a[$1] += 1;} 
    END {
        for (i in a) printf("%s adalah IP yang paling banyak mengakses server yakni dengan requests sebanyak %d\n", i, a[i]);}' newlog.log | sort -n | tail -n 1 > result.txt

Untuk memudahkan proses scan dengan awk maka sebelumnya digunakan command sed untuk menggantikan seluruh tanda petik di file log dengan spasi/blank dan hasil sed dimasukkan ke file log baru newlog.log

Kemudian digunakan awk pada file newlog.log dengan melakuan increment untuk setiap variabel $1, pada kasus ini alamat IP, yang ditemukan (bersifat unik, dalam artian tiap IP berbeda memiliki nilai yang terpisah) kemudian disimpan didalam array a yang selanjutnya akan diprint beserta alamat IP yang memiliki nilai tertinggi setelah dilakukan proses sort dan tail untuk mengurutkan nilai tinggi ke rendah dan hanya mencetak 1 nilai di paling atas, kemudian hasilnya dimasukkan ke file result.txt yang belum dipindahkan karena masih akan digunakan untuk subsoal berikutnya.

## Kendala
Kendala yang dialami saat mengerjakan soal ini adalah sulitnya untuk melakukan scan pada file log yang asli karena tanda petik menyebabkan scan variabel $1 menjadi sebaris data yang tidak relevan, untuk solusinya digunakan command sed dan membentuk file baru tanpa tanda petik. 

# 2d.

## Analisa 
Pada subsoal ini script diminta menemukan jumlah IP yang menggunakan curl sebagai user agent dan menyimpan hasilnya di file result.txt.

## Penyelesaian
    awk '
    /curl/{++jumlah_req_curl}
    END {
        print "Ada", jumlah_req_curl, "requests yang menggunakan curl sebagai user-agent"
    }
    ' log_website_daffainfo.log > result2.txt
    cat result2.txt >> result.txt

Penyelesaiannya adalah dengan menggunakan awk sederhana dimana kondisi scan adalah /curl/ dan aksi nya ++jumlah_req_curl. Sehingga untuk setiap curl yang ditemukan didalam log akan menambahkan nilai pada variabel jumlah_req_curl. Kemudian hasilnya akan diprint dan dimasukkan ke file result2.txt kemudian dengan command cat akan digabungkan ke file result.txt.

## Kendala
Kendala yang dialami sebelumnya adalah saat memasukkan hasil awk ke result.txt hasil dari awk sebelumnya hilang tergantikan. Sehingga solusinya adalah dengan membuat file baru result2.txt kemudian dengan command cat menyatukannya dengan result.txt.

# 2e.

## Analisa
Pada subsoal ini script diminta mencari dan mencetak IP IP yang melakukan serangan pada pukul 2 pagi dan dimasukkan ke result.txt.

## Penyelesaian
    awk '
    /2022:02/{print $1}
    ' newlog.log > result3.txt
    cat result3.txt >> result.txt

Penyelesaiannya adalah dengan menggunakan awk untuk melakukan scan dengan kriteria /2022:02/ dan aksi print $1. Sehingga setiap menemukan serangan/request pada pukul 02:00 di log akan dicetak variabel $1, dalam kasus ini IP address, dari barisan request tersebut. Dengan demikian seluruh request pada jam tersebut akan tertampil IP addressnya. Kemudian hasil disimpan ke result3.txt yang disatukan dengan result.txt menggunakan cat.

# Screenshot 
Isi dari file ratarata.txt dan result.txt di dalam folder forensic_log_website_daffainfo_log setelah pengeksekusian script berhasil.
![](img/2.1.png) 

![](img/2.2.png) 

![](img/2.3.png) 

# Soal 3

# Soal 3a

## Analisa Soal
Pada soal ini, kami diminta untuk membuat script dengan format minute_log.sh yang memasukkan metrics ke dalam file log dengan format metrics_{YmdHms}.log. {YmdHms} adalah waktu saat file dijalankan. Contoh isi dari file metrics tersebut :
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

## Penyelesaian
Dibuat direktori untuk menyimpan file log

![](img/3.1.png) 

Pada script minute_log.sh untuk isi dari metrics untuk mem dan swap didapat dari command free -m sedangkan untuk path dan path size didapat dari du -sh /home/namauser

![](img/3.2.png)

Pertama dideklarasikan variabel datenow untuk mengambil tanggal dan juga waktu untuk penamaan file dan variabel path untuk tempat menyimpan file

![](img/3.3.png) 

Kemudian dijalankan printf untuk mencetak string di line pertama pada file log.  Pada line berikutnya digunakan awk untuk mengambil informasi dari command free-m dan juga du -sh /home/namauser. Pada awk untuk mem dan swap input yang diambil dimulai  dari yang kedua ($2) karena input yang pertama adalah Mem: dan Swap: . Kemudian untuk path dan path size dimulai dari yang kedua lalu yang pertama karena yang kedua merupakan path dan yang pertama adalah path size

![](img/3.4.png)

# Soal 3b

## Analisa Soal
Pada soal ini kami diminta agar script yang sudah dibuat berjalan otomatis tiap menit

## Penyelesaian
Digunakan cron agar script berjalan secara otomatis. Pertama kita install cron dengan command

![](img/3.5.png) 

Kalau sudah selesai dijalankan

![](img/3.6.png)
 
Maka akan terbuka terminal cron, lalu dimasukkan

![](img/3.7.png)
 
Lalu save.

# Soal 3c

# Soal 3d

## Analisa Soal
Pada soal ini kami diminta agar file log hanya dapat dibaca oleh pemilik file

## Penyelesaian
Kami menggunakan chmod untuk mengubah permission access ke file.  Command tersebut kita tambahkan di akhir script
 
![](img/3.8.png)
